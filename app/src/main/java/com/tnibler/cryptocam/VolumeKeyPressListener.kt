package com.tnibler.cryptocam

interface VolumeKeyPressListener {
    fun onVolumeKeyDown()
}